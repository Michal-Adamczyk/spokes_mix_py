# usr/bin/env python

# -*- coding: utf-8 -*-

# Standard library imports
import multiprocessing
import logging

# Local imports
from parser import Parser
from spokes_object import SpokesObjectCompiler, SpokesContainer


def get_args():
    parser = Parser.create_parser()
    args = parser.parse_args()
    return args


def get_container(input_dir):
    container = SpokesContainer(input_dir)
    container.add_spokes_objects()
    return container


def get_annotation(obj, tier, bar_position, safe, remote):
    logging.info(f'Started processing {obj}')
    compiler = SpokesObjectCompiler(obj, tier, bar_position, safe, remote)
    compiler.compile()
    logging.info(f'Finished processing {obj}')


if __name__ == '__main__':
    logging.basicConfig(filename='spokes.log', level=logging.DEBUG)
    logging.info('Started')
    args = get_args()
    container = get_container(args.input_dir)
    objects = container.spokes_objects
    process_number = iter(list(range(len(objects))))
    pool = multiprocessing.Pool(2)
    for obj in objects:
        # get_annotation(obj, args.tier, next(process_number), args.safe, args.remote)
        pool.apply_async(get_annotation, args=(obj, args.tier,
                         next(process_number), args.safe, args.remote))
    pool.close()
    pool.join()
    logging.info('Finished')
