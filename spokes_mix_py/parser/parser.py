# Standard library imports
import argparse
import ast
import os


class Parser:

    @classmethod
    def create_parser(cls):
        parser = argparse.ArgumentParser(description='Add word and sound\
                                        annotation to .eaf files')
        parser.add_argument('-i',
                            '--input_dir',
                            help='Relative directory with input files',
                            required=True,
                            type=cls.check_if_dir_exists)

        parser.add_argument('-t',
                            '--tier',
                            help='Name of the tier to recover. Default is set to all',
                            required=False,
                            default='all')

        parser.add_argument('-s',
                            '--safe',
                            help='Add -S False to run the program without safety checks\
                                (Reduces prcessing time)',
                            type=ast.literal_eval,
                            default=True)

        parser.add_argument('-r',
                            '--remote',
                            help='Add -r to True to run the program using remote Clarin services',
                            type=ast.literal_eval,
                            default=False)

        return parser

    @classmethod
    def check_if_dir_exists(cls, dirname):
        if not os.path.isdir(dirname):
            error_msg = 'Directory does not exist!'
            raise SystemExit(error_msg)
        else:
            return dirname
