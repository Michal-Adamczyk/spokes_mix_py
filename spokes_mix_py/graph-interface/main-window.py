from kivy.app import App
from kivy.uix.label import Label


class SpokesApp(App):

    def build(self):
        return Label(text='Spokes App')


if __name__ == '__main__':
    app = SpokesApp()
    app.run()
