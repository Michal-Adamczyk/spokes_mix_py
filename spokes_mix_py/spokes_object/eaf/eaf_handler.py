# Standard library
from collections import namedtuple
import os

# Non-standard imports
from pympi.Elan import Eaf


class EafAnnotationHandler:

    def __init__(self, obj, tier):
        self.obj = obj
        self.tier = tier
        self.parser = Eaf
        self.annotation_schema = namedtuple('annotation', ['tier', 'start', 'end', 'text'])

    def parse_eaf(self):
        self.get_parsed_eaf()
        self.serialize_annotation()
        self.add_new_eaf_tiers()

    def get_parsed_eaf(self):
        eaf_path = os.path.join(self.obj.root, self.obj.eaf_file)
        self.obj.parsed_eaf = self.parser(eaf_path)

    def serialize_annotation(self):
        if self.tier.lower() == 'all':
            self.get_all_tiers()
        elif self.tier in self.obj.parsed_eaf.get_tier_names():
            self.get_one_tier()
        else:
            raise ValueError(f'{self.tier} tier cannot be found')

    def get_one_tier(self):
        for start, end, text in self.obj.parsed_eaf.get_annotation_data_for_tier(self.tier):
            text = text.replace(u'\xa0', u' ')
            schema = self.annotation_schema(self.tier, start, end, text)
            self.obj.eaf_annotation.append(schema)

    def get_all_tiers(self):
        for tier in self.obj.parsed_eaf.get_tier_names():
            if tier != 'events':
                for start, end, text in self.obj.parsed_eaf.get_annotation_data_for_tier(tier):
                    text = text.replace(u'\xa0', u' ')
                    schema = self.annotation_schema(tier, start, end, text)
                    self.obj.eaf_annotation.append(schema)

    def add_new_eaf_tiers(self):
        tiers = {annot[0] for annot in self.obj.eaf_annotation}
        tiers_to_add = []
        for tier in tiers:
            tiers_to_add.append(tier + '_word')
            tiers_to_add.append(tier + '_phon')
        for tier in tiers_to_add:
            self.obj.parsed_eaf.add_tier(tier)
