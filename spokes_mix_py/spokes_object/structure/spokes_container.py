# Standard library imports
import os
import re

# Local imports
from .spokes_object import SpokesObject


class SpokesContainer:
    def __init__(self, input_dir):
        self.input_dir = input_dir
        self.spokes_objects = self.add_spokes_objects()

    def add_spokes_objects(self):
        spokes_objects = []
        for root, dirs, files in self.get_dir_structure(self.input_dir):
            if root != self.input_dir:
                files = self.filter_folder(files)
                self.check_if_one_eaf_wave_file_in_folder(files)
                spokes_object = SpokesObject()
                spokes_object.root = root
                spokes_object.eaf_file = self.get_file_name(files, 'eaf')
                spokes_object.wave_file = self.get_file_name(files, 'wav')
                spokes_objects.append(spokes_object)
        return spokes_objects

    def get_dir_structure(self, directory):
        dir_structure = os.walk(directory, topdown=False)
        return dir_structure

    def filter_folder(self, files):
        reg_ex = re.compile('(.*.eaf|.*.wav|.*.EAF|.*.WAV)')
        filtered_files = tuple(filter(reg_ex.match, files))
        return filtered_files

    def check_if_one_eaf_wave_file_in_folder(self, files):
        regex_wav = re.compile('.*.wav|.*.WAV')
        regex_eaf = re.compile('.*.eaf|.*.EAF')
        wave_files = tuple(filter(regex_wav.match, files))
        eaf_files = tuple(filter(regex_eaf.match, files))
        if (len(wave_files) != 1) or (len(eaf_files) != 1):
            raise ValueError("There should be one .eaf and one .wav files.\
                             in the folder")

    def get_file_name(self, files, extension):
        if extension.lower() in {'eaf', 'wav'}:
            reg_ex = re.compile(f'.*.{extension.lower()}|.*.{extension.upper()}')
            files = list(filter(reg_ex.match, files))
            return files[0]
        else:
            raise ValueError('Wrong extension. Choose eaf or wav')
