class SpokesObject:

    __slots__ = ['root',
                 'eaf_file',
                 'wave_file',
                 'parsed_eaf',
                 'eaf_annotation',
                 'split_wave_files',
                 'segmented_annotation',
                 'final_eaf']

    def __init__(self, root='', eaf_file='', wave_file='', parsed_eaf='',
                 eaf_annotation=None, split_wave_files=None,
                 segmented_annotation=None, final_eaf=''):

        self.root = root
        self.eaf_file = eaf_file
        self.wave_file = wave_file
        self.parsed_eaf = parsed_eaf
        self.eaf_annotation = eaf_annotation if eaf_annotation is not None else []
        self.split_wave_files = split_wave_files if split_wave_files is not None else []
        self.segmented_annotation  = segmented_annotation if segmented_annotation is not None else []
        self.final_eaf = final_eaf

    def __str__(self):
        return 'SpokesObject ' + self.root
