class FileProcessingError(Exception):
    "Raised when HttpResponse returns error message"
    pass
