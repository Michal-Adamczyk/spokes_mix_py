# Standard library imports
from json import JSONDecodeError
import json
import logging


class CtmParser:
    def __init__(self, obj):
        self.obj = obj

    def parse(self, output, annotation):
        self.add_words_tier_to_eaf(output, annotation)
        self.add_phons_tier_to_eaf(output, annotation)

    def add_words_tier_to_eaf(self, output, annotation):
        tier_name = annotation.tier + '_word'
        for line in output.splitlines():
            if not line.startswith('@'):
                l = line.split()
                start = annotation.start + int(round(float(l[2]), 4) * 1000)
                self.obj.parsed_eaf.add_annotation(
                    tier_name,
                    start,
                    start + int(round(float(l[3]), 4) * 1000),
                    value=l[4])

    def add_phons_tier_to_eaf(self, output, annotation):
        tier_name = annotation.tier + '_phon'
        for line in output.splitlines():
            if line.startswith('@'):
                l = line.split()
                if l[4] != 'sil':
                    start = annotation.start + int(round(float(l[2]), 4) * 1000)
                    self.obj.parsed_eaf.add_annotation(
                        tier_name,
                        start,
                        start + int(round(float(l[3]), 4) * 1000),
                        value=l[4])


class JsonParser:
    def __init__(self, obj):
        self.obj = obj

    def parse(self, json_file, annotation):
        try:
            parsed_json = json.loads(json_file)
            self.add_words_tier_to_eaf(parsed_json, annotation)
            self.add_phons_tier_to_eaf(parsed_json, annotation)
        except JSONDecodeError as e:
            logging.info(f'{e}')
            logging.info('Faulty input. Unable to parse JSON file\n')

    def add_words_tier_to_eaf(self, parsed_json, annotation):
        tier_name = annotation.tier + '_word'
        for i, item in enumerate(parsed_json['levels'][1]['items']):
            start = annotation.start + (item['sampleStart'] // 16)
            if item['labels'][0]['value'] != '':
                self.obj.parsed_eaf.add_annotation(
                    tier_name,
                    start,
                    start + (item['sampleDur'] // 16),
                    value=item['labels'][0]['value'])

    def add_phons_tier_to_eaf(self, parsed_json, annotation):
        tier_name = annotation.tier + '_phon'
        for i, item in enumerate(parsed_json['levels'][2]['items']):
            start = annotation.start + (item['sampleStart'] // 16)
            if item['labels'][0]['value'] != 'sil':
                self.obj.parsed_eaf.add_annotation(
                    tier_name,
                    start,
                    start + (item['sampleDur'] // 16),
                    value=item['labels'][0]['value'])
