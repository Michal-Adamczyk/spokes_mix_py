# Standard library imports
import os
import requests
import logging


class HttpRequest:

    def __init__(self):
        self.URLS = {'align':             'http://mowa.clarin-pl.eu/tools/speech/forcealign',
                     'download':          'http://mowa.clarin-pl.eu//tools/download',
                     'download_json':     'http://mowa.clarin-pl.eu//tools/annot/',
                     'normalize':         'http://mowa.clarin-pl.eu/tools/audio/normalize',
                     'status':            'http://mowa.clarin-pl.eu//tools/status',
                     'upload_audio':      'http://mowa.clarin-pl.eu/tools/upload/audio',
                     'upload_transcript': 'http://mowa.clarin-pl.eu/tools/upload/transcript'}
        self.session = requests.Session()

    def get_request(self, command, files):
        command = command.lower()
        url = self.get_url(command)
        if command in {'align', 'download', 'download_json', 'status'}:
            form = self.get_form(id=files)
            session = self.session.get(os.path.join(url, form), timeout=5)
            logging.info(f'{command} :: {session.headers}')
            return session
        elif command in {'normalize', 'upload_audio', 'upload_transcript'}:
            form = self.get_form(file=files)
            session = self.session.post(url, files=form, timeout=5)
            logging.info(f'{command} :: {session.headers}')
            return session
        else:
            raise ValueError('Provided command is incorrect')

    def get_url(self, command):
        for cmd in self.URLS.keys():
            if command == cmd:
                url = self.URLS[f'{cmd}']
                return url
        raise ValueError('Provided command has no specified url.')

    def get_form(self, id=None, file=None):
        if id is not None:
            return id
        else:
            form = self.get_dict_form(file)
            return form

    def get_dict_form(self, file):
        if file is not None:
            form = {}
            form['file'] = ('file', file)
            return form
        raise AttributeError('Missing request input. Specify id or file')
