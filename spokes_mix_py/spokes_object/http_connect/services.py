# Standard library imports
from abc import ABC, abstractmethod
from subprocess import Popen, STDOUT, PIPE
from tempfile import mkstemp
import json
import logging
import os
import time
import wave

# Non-standard imports
from requests.exceptions import ConnectionError, ReadTimeout
from tqdm import tqdm


class SegmentatorABC(ABC):
    def __init__(self,
                 obj,
                 parsing_strategy,
                 request_marshaller,
                 bar_position,
                 safety):
        self.obj = obj
        self.safety = safety
        self.bar_position = bar_position
        self.parsing_strategy = parsing_strategy(self.obj)
        self.request_marshaller = request_marshaller()

    def segment_annotation(self):
        files = zip(self.obj.split_wave_files, self.obj.eaf_annotation)
        with tqdm(total=len(self.obj.eaf_annotation), position=self.bar_position) as bar:
            bar.set_description(f'{self.obj}')
            for sound_bite, annotation_line in files:
                self.request(sound_bite, annotation_line)
                bar.update(1)

    @abstractmethod
    def request(self, sound, annotation):
        pass

class SegmentatorDocker(SegmentatorABC):
    def request(self, sound, annotation):
        wav_handle, wav_tmp = mkstemp(dir=self.obj.root, suffix='.wav')
        txt_handle, txt_tmp = mkstemp(dir=self.obj.root, suffix='.txt')
        align_handle, align_tmp = mkstemp(dir=self.obj.root, suffix='.ctm')
        wav_obj = wave.open(wav_tmp, 'w')
        wav_obj.setparams(sound.params)
        wav_obj.writeframes(sound.frames)
        wav_obj.close()
        txt_obj = os.fdopen(txt_handle, 'w')
        txt_obj.write('{}'.format(annotation.text))
        txt_obj.close()
        w = os.path.basename(wav_tmp)
        t = os.path.basename(txt_tmp)
        a = os.path.basename(align_tmp)
        cmd_rec = ['docker',
                   'run',
                   '--rm', '-v',
                   f'{os.path.dirname(wav_tmp)}:/data',
                   'danijel3/clarin-pl-speechtools:studio',
                   f'/tools/Recognize/run.sh {w} {t}']
        cmd_force = ['docker',
                     'run',
                     '--rm', '-v',
                     f'{os.path.dirname(wav_tmp)}:/data',
                     'danijel3/clarin-pl-speechtools:studio',
                     f"/tools/ForcedAlign/run.sh {w} {t} {a}"]
        proc_rec = Popen(cmd_rec, stdout=PIPE, stderr=STDOUT)
        proc_force = Popen(cmd_force, stdout=PIPE, stderr=STDOUT)
        proc_rec.communicate()
        proc_force.communicate()
        with open(align_tmp, 'r') as f:
            out = f.read()
        os.remove(wav_tmp)
        os.remove(txt_tmp)
        os.remove(align_tmp)
        self.parsing_strategy.parse(out, annotation)

class Segmentator(SegmentatorABC):
    def request(self, sound, annotation):
        return_value_sound = self.make_request('upload_audio', sound.output)
        return_value_text = self.make_request('upload_transcript', annotation.text)
        if self.safety:
            self.wait_for_ok_status(return_value_sound, annotation)
            self.wait_for_ok_status(return_value_text, annotation)
        required_response = os.path.join(return_value_sound, return_value_text)
        self.request_align(required_response, annotation)

    def request_align(self, response, annotation):
        output = self.make_request('align', response)
        if self.safety:
            self.wait_for_ok_status(output, annotation)
        self.get_output(output, annotation)

    def get_output(self, response, annotation):
        json_valid = False
        count = 0
        while not json_valid and count < 5:
            try:
                output = self.make_request('download_json', response)
                count += 1
                assert json_valid != self.is_json_valid(output)
                json_valid = True
            except AssertionError as e:
                logging.info(f'{self.obj} :: Invalid JSON format')
                time.sleep(2)
        self.parse_json(output, annotation)

    def is_json_valid(self, json_file):
        try:
            jf = json.loads(json_file)
        except ValueError as e:
            logging.info(f'{self.obj} :: {e}')
            return False
        return True

    def retry_on_failure(method):
        def retry_request(self, *args, **kwargs):
            request_connected = False
            while not request_connected:
                try:
                    output = method(self, *args, **kwargs)
                    request_connected = True
                    return output
                except ReadTimeout:
                    time.sleep(2)
                    logging.info(f'{self.obj} :: Read timeout')
                except ConnectionError:
                    time.sleep(2)
                    logging.info(f'{self.obj} :: Connection Error')
        return retry_request

    @retry_on_failure
    def make_request(self, request_type, form):
        if request_type == 'download_json':
            output = self.request_marshaller.get_request(request_type, form).content
        else:
            output = self.request_marshaller.get_request(request_type, form).text
        return output

    def parse_json(self, json_file, annotation):
        self.parsing_strategy.parse(json_file, annotation)

    def wait_for_ok_status(self, response, annotation):
        status = self.request_marshaller.get_request('status', response)
        while status.text != 'ok':
            if status.text == 'wait':
                status = self.request_marshaller.get_request('status', response)
            else:
                logging.info(f'{self.obj} :: {status.text}')
                logging.info(f'{self.obj} :: {annotation.text}')
                break
