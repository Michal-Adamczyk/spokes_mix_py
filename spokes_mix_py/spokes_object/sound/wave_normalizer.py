# Standard library imports
import os

# Non-standard imports
from pydub import AudioSegment


class WaveNormalizer:

    def __init__(self, obj):
        self.obj = obj

    def normalize(self):
        self.normalize_wave_file()

    def normalize_wave_file(self):
        file = os.path.join(self.obj.root, self.obj.wave_file)
        with open(file, 'rb') as f:
            raw_wave = AudioSegment.from_file(file, format='wav')
        raw_wave = raw_wave.set_channels(1)
        raw_wave = raw_wave.set_frame_rate(16000)
        with open(file, 'wb') as f:
            raw_wave.export(f, format='wav')
