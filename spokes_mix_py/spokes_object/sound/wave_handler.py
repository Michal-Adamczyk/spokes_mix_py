# Standard library imports
import os
import uuid
import wave
from collections import namedtuple


class WaveHandler:

    def __init__(self, obj):
            self.obj = obj
            self.annotation_schema = namedtuple('sound', ['output', 'params', 'frames'])

    def split_wave(self):
        file = os.path.join(self.obj.root, self.obj.wave_file)
        with wave.open(file, 'rb') as in_file:
            tmp_wav = str(uuid.uuid4())
            for annotation in self.obj.eaf_annotation:
                tier, start, end, text = annotation
                with wave.open(f'{tmp_wav}.wav', 'wb') as tmp_file:
                    start, end, nframes = self.get_wave_start_end_nframes(start, end)
                    params = in_file.getparams()
                    in_file.setpos(start)
                    frames = in_file.readframes(nframes)
                    tmp_file.setparams(params)
                    tmp_file.writeframes(frames)
                    tmp_file.close()
                    self.store_file(tmp_wav, params, frames)
        os.remove(f'{tmp_wav}.wav')

    def get_wave_start_end_nframes(self, start, end):
        start = int(start * 16000 / 1000)
        end = int(end * 16000 / 1000)
        nframes = end - start
        return (start, end, nframes)

    def store_file(self, tmp_wav, params, frames):
        with open(f'{tmp_wav}.wav', 'rb') as tmp_file:
            output = tmp_file.read()
            schema = self.annotation_schema(output, params, frames)
            self.obj.split_wave_files.append(schema)
