# Standard library imports
import os


class SaveEaf:

    def __init__(self, obj):
        self.obj = obj

    def save_to_file(self):
        out_file = os.path.join(self.obj.root, 'out.eaf')
        self.obj.parsed_eaf.to_file(out_file)
