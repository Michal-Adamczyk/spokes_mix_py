# Local imports
from .eaf import EafAnnotationHandler
from .http_connect import Segmentator, SegmentatorDocker
from .http_connect.parser import JsonParser, CtmParser
from .persistence import SaveEaf
from .sound import WaveHandler, WaveNormalizer
from .http_connect.http_request import HttpRequest


class SpokesObjectCompiler:

    def __init__(self, obj, tier, bar_position, *args):
        self.obj = obj
        self.eaf_annotation_handler = EafAnnotationHandler(self.obj, tier)
        self.wave_normalizer = WaveNormalizer(self.obj)
        self.wave_handler = WaveHandler(self.obj)
        if args[1]:  # If True run remote services
            self.segmentator = Segmentator(self.obj, JsonParser, HttpRequest, bar_position, args[0])
        else:
            self.segmentator = SegmentatorDocker(self.obj, CtmParser, HttpRequest, bar_position, args[0])
        self.persistence = SaveEaf(self.obj)

    def compile(self):
        self.eaf_annotation_handler.parse_eaf()
        self.wave_normalizer.normalize()
        self.wave_handler.split_wave()
        self.segmentator.segment_annotation()
        self.persistence.save_to_file()

    def save_to_file(self):
        pass
