# Python tools for SpokesMix

## Description

The program enables automatic time annotation of EAF files for those sources which contain only manual annotation of utterance boundaries. It utilizes [CLARIN-PL speech processing services](https://mowa.clarin-pl.eu). On the basis of manual annotation included in the entry EAF and audio WAV file, it returns a new EAF with pre-existing manual annotation of utterance boundaries and newly-added word and phonetic annotation layers. The program was written in Python 3.6, which is required for the program to execute correctly. Current source code and documentation are available in an open repository: https://gitlab.com/Michal-Adamczyk/spokes_mix_py.git. The program is used through a command line interface. To launch the program, use to following command:

```
python main.py -i INPUT_DIR [-t TIER] [-s SAFE] [-r REMOTE]
```

The -i flag allows the user to specify the directory containing pairs of WAV and EAF files. Each pair has to be placed in a separate folder. The -t flag allows the user to restrict processing to a concrete interlocutor. The -s flag serves turns off safety locks used in communication with the CLARIN-PL speech processing services in order to speed up the program. The -t flag defaults to all interlocutors. The -s flag keeps all safety locks on by default. In order to turn off safety locks, pass "False" to the -s flag. The -r flag switches between local copy of the services and their remote equivalents.

Examples of command-line calls:

* Basic call:
```
python main.py -i path/to/your/directory
```
* Call restricted to a particular interlocutor:
```
python main.py -i path/to/your/directory -t invee
```
* Call without safety locks:
```
python main.py -i path/to/your/directory -s False
```

* By default, the program works on a dockerized services provided by (https://mowa.clarin-pl.eu). You can set an -r flag to True and run remote services.
```
python main.pu -i path/to/your/directory -r True
```
## Requirements

* Install the [Conda package manager](https://conda.io) on your computer.

## Setup

* Create virtual environment by typing:

```
conda create -n spokes_mix python=3.6
```

* Activate the environment:

```
source activate spokes_mix
```

* Update the environment:

```
conda env update -n spokes_mix -f environment.yml
```

## Usage

* Run the program:

```
python main.py -i INPUT_DIR [-t TIER] [-s SAFE] [-r REMOTE]
```

```
INPUT_DIR: A pair of EAF and WAVE files has to be in a separate folder
```

```
TIER: Pass tier name; default is set to ALL; 'Events' tier is skipped by default.
```

```
SAFE: Add -s False to turn off security locks. It speeds up segmentation, but it is not always stable. Use at your own risk.
```

```
REMOTE Add -r True to start the program querying the remote server.
```
