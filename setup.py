from setuptools import setup

setup(
    name='spokes_mix_py',
    version='0.0.0',
    packages=['spokes_mix_py'],
    author='Piotr Pezik, Michal Adamczyk, et al.',
    author_email='michaladamczyk@icloud.com',
    description='Adding word and sound annotation to existing EAF file',
)
